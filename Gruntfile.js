/*
 * grunt-mingos-markdown-py
 * https://bitbucket.org/mingos/grunt-markdown-py.git
 *
 * Copyright (c) 2014 Dominik Marczuk
 * Licensed under the BSD-3-Clause license.
 */

'use strict';

module.exports = function (grunt) {

	// Project configuration.
	grunt.initConfig({
		jshint: {
			all: [
				'Gruntfile.js',
				'tasks/*.js',
				'<%= nodeunit.tests %>'
			],
			options: {
				jshintrc: '.jshintrc'
			}
		},

		// Before generating any new files, remove any previously-created files.
		clean: {
			tests: ['tmp']
		},

		// Configuration to be run (and then tested).
		markdown_py: {
			default_options: {
				options: {},
				files: [
					{ expand: true, src: ["**/*.md"], dest: "tmp/", cwd: "test/fixtures/" }
				]
			},
			custom_options: {
				options: {
					extension: "htm",
					md_options: {
						extension: [
							"extra",
							"smarty"
						]
					}
				},
				files: [
					{ expand: true, src: ["**/*.md"], dest: "tmp/", cwd: "test/fixtures/" }
				]
			}
		},

		// Unit tests.
		nodeunit: {
			tests: ['test/*_test.js']
		}

	});

	// Actually load this plugin's task(s).
	grunt.loadTasks('tasks');

	// These plugins provide necessary tasks.
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-nodeunit');

	// Whenever the "test" task is run, first clean the "tmp" dir, then run this
	// plugin's task(s), then test the result.
	grunt.registerTask('test', ['clean', 'markdown_py', 'nodeunit', 'clean']);

	// By default, lint and run all tests.
	grunt.registerTask('default', ['jshint', 'test']);

};
