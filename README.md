# grunt-mingos-markdown-py

> Convert Markdown files to HTML using [mingos-markdown-py](https://www.npmjs.org/package/mingos-markdown-py).

## Getting Started
This plugin requires Grunt `~0.4.5`

If you haven't used [Grunt](http://gruntjs.com/) before, be sure to check out the
[Getting Started](http://gruntjs.com/getting-started) guide, as it explains how to create a
[Gruntfile](http://gruntjs.com/sample-gruntfile) as well as install and use Grunt plugins. Once you're familiar with
that process, you may install this plugin with this command:

```shell
npm install grunt-mingos-markdown-py --save-dev
```

Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:

```js
grunt.loadNpmTasks('grunt-mingos-markdown-py');
```

## The "markdown_py" task

### Overview
In your project's Gruntfile, add a section named `markdown_py` to the data object passed into `grunt.initConfig()`.

```js
grunt.initConfig({
  markdown_py: {
    options: {
      // Task-specific options go here.
    },
    your_target: {
      // Target-specific file lists and/or options go here.
    },
  },
});
```

### Options

#### options.extension
Type: `String`
Default value: `'html'`

The extension of the output files.

#### options.md_options
Type: `Object`
Default value: `{}`

Options passed in to the [mingos-markdown-py](https://www.npmjs.org/package/mingos-markdown-py#additional-options)
module, as they would be passed in to its `convert()` method. 

### Usage Examples

#### Default Options
In this example, the default options are used to convert Markdown files present in the directory "source/" to HTML and
save them to the "destination/" directory. Note that the file paths and names will be retained, only the ".md"
extensions will be changed to ".html".

```js
grunt.initConfig({
  markdown_py: {
    options: {},
    files: [
      { expand: true, src: ['source/**/*.md'], dest: 'destination/' }
    ]
  }
});
```

#### Custom Options
In this example, custom options are used to alter the output of mingos-markdown-py by telling it to use extensions:

```js
grunt.initConfig({
  markdown_py: {
    options: {
      extension: [
        'extra',
        'smarty'
      ]
    },
    files: [
      { expand: true, src: ['source/**/*.md'], dest: 'destination/' }
    ],
  },
});
```
