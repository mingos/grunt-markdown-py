/*
 * grunt-mingos-markdown-py
 * https://bitbucket.org/mingos/grunt-markdown-py.git
 *
 * Copyright (c) 2014 Dominik Marczuk
 * Licensed under the BSD-3-Clause license.
 */

"use strict";

var md = require("mingos-markdown-py");

var replaceExtension = function(filename, extension) {
	filename = filename.split(".");
	filename[filename.length - 1] = extension;

	return filename.join(".");
};

module.exports = function(grunt) {
	grunt.registerMultiTask("markdown_py", "Convert Markdown files to HTML using mingos-markdown-py.", function() {
		// this is an async task
		var done = this.async();

		// default options
		var options = this.options({
			extension: "html",
			md_options: {}
		});

		// input files
		var files = this.files;

		// processed files count, result
		var processed = 0, failed = 0, result = true;
		var checkDone = function() {
			if (processed + failed === files.length) {
				done(result);
				grunt.log.writeln("Created " + processed + " files.");
			}
		};

		// Iterate over all files
		files.forEach(function(f) {
			var src = f.src;

			// only one file allowed
			if (src.length > 1) {
				grunt.log.warn("Cannot convert multiple source files into one output file.");
				failed++;
				result = false;
				return;
			}

			src = src[0];

			// file must exist
			if (!grunt.file.exists(src)) {
				grunt.log.warn("Source file \"" + src + "\" not found.");
				failed++;
				result = false;
				return;
			}

			// change the destination file's extension
			var dest = replaceExtension(f.dest, options.extension);

			// convert markdown to HTML and save the result
			md.convert(src, options.md_options, function(output) {
				grunt.file.write(dest, output);
				processed++;

				checkDone();
			});
		});

		checkDone();
	});

};
